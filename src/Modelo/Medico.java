package Modelo;

import java.sql.SQLException;

import javax.swing.table.DefaultTableModel;

public class Medico extends Usuario  {

	private String especialidad;
	private Integer estado;
	private static Medico bd = null;

	public Medico(String especialidad, Integer estado) {
		super();
		this.especialidad = especialidad;
		this.estado = estado;
	}

	public Medico() {

	}

	public static Medico getInstance() {

		if (bd == null) {
			bd = new Medico();
		}
		return bd;

	}
	
	
	

	public String getEspecialidad() {
		return especialidad;
	}

	public void setEspecialidad(String especialidad) {
		this.especialidad = especialidad;
	}

	public Integer getEstado() {
		return estado;
	}

	public void setEstado(Integer estado) {
		this.estado = estado;
	}

	public Integer obtenerTipoUsuario(String nombreUsuario) {
		return bd.obtenerTipoUsuario(nombreUsuario);
	}

	public boolean altaUsuario(Usuario c) {
		return bd.altaUsuario(c);
	}

	public boolean eliminarUsuario(String nombre) throws SQLException {
		return bd.eliminarUsuario(nombre);
	}

	public Boolean modificarUsuario(Usuario u, Integer id) {
		return bd.modificarUsuario(u, id);
	}

	public Integer obteneridUsuario(String dni) {
		return bd.obteneridUsuario(dni);
	}

	public Usuario datosUsuario(Integer id) {
		return bd.datosUsuario(id);
	}

	public DefaultTableModel actualizarTablaUsuario(String busqueda) {
		return bd.actualizarTablaUsuario(busqueda);
	}

}
