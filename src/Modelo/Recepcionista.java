package Modelo;

import java.sql.SQLException;

import javax.swing.table.DefaultTableModel;

public class Recepcionista extends Usuario{
	
	private static Recepcionista bd = null;

	public Recepcionista() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Recepcionista(Integer dni, String nombreUsuario, String nombre, String apellido, String contrasenia,
			String email, String telefono, String domicilio, String localidad, Integer tipoUsuario) {
		super(dni, nombreUsuario, nombre, apellido, contrasenia, email, telefono, domicilio, localidad, tipoUsuario);
		// TODO Auto-generated constructor stub
	}
	
	public static Recepcionista getInstance() {

		if (bd == null) {
			bd = new Recepcionista();
		}
		return bd;

	}

	public Boolean login(String nombreUsuario, String contrasenia) {
		return bd.login(nombreUsuario, contrasenia);
	}

	public Integer obtenerTipoUsuario(String nombreUsuario) {
		return bd.obtenerTipoUsuario(nombreUsuario);
	}

	public boolean altaUsuario(Usuario c) {
		return bd.altaUsuario(c);
	}

	public boolean eliminarUsuario(String nombre) throws SQLException {
		return bd.eliminarUsuario(nombre);
	}

	public Boolean modificarUsuario(Usuario u, Integer id) {
		return bd.modificarUsuario(u, id);
	}

	public Integer obteneridUsuario(String dni) {
		return bd.obteneridUsuario(dni);
	}

	public Usuario datosUsuario(Integer id) {
		return bd.datosUsuario(id);
	}

	public DefaultTableModel actualizarTablaUsuario(String busqueda) {
		return bd.actualizarTablaUsuario(busqueda);
	}
	
	
	
	

}
