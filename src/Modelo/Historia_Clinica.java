package Modelo;

public class Historia_Clinica {

	private Integer nroHistorial;
	private Paciente paciente;
	private Medico medico;
	private static Historia_Clinica bd =null;

	public Historia_Clinica(Integer nroHistorial, Paciente paciente, Medico medico) {
		super();
		this.nroHistorial = nroHistorial;
		this.paciente = paciente;
		this.medico = medico;
	}
	
	public Historia_Clinica() {
		
	}
	
	public static Historia_Clinica getInstance() {

		if (bd == null) {
			bd = new Historia_Clinica();
		}
		return bd;

	}
	
	

	public Integer getNroHistorial() {
		return nroHistorial;
	}

	public void setNroHistorial(Integer nroHistorial) {
		this.nroHistorial = nroHistorial;
	}

	public Paciente getPaciente() {
		return paciente;
	}

	public void setPaciente(Paciente paciente) {
		this.paciente = paciente;
	}

	public Medico getMedico() {
		return medico;
	}

	public void setMedico(Medico medico) {
		this.medico = medico;
	}

}
