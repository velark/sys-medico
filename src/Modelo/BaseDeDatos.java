package Modelo;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;

import javax.swing.table.DefaultTableModel;

public class BaseDeDatos {

	/** La conexion con la base de datos */
	private Connection conexion = null;
	private static BaseDeDatos bd = null;

	/** Se establece la conexion con la base de datos */

	private BaseDeDatos() {
		estableceConexion();
	}

	private void estableceConexion() {
		if (conexion != null)
			return;

		try {
			// Se registra el Driver de MySQL
			DriverManager.registerDriver(new com.mysql.cj.jdbc.Driver());

			// Se obtiene una conexi�n con la base de datos. Hay que
			// cambiar el usuario "usuario" y la clave "" por las
			// adecuadas a la base de datos que estemos usando.
			conexion = DriverManager.getConnection("jdbc:mysql://localhost/TF_consultorio", "root", "cacorucho");
			// conexion = DriverManager.getConnection(
			// "jdbc:mysql://172.17.111.138/ex000377_afip","ex000377_afip","paLEpili01");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static BaseDeDatos getInstance() {
		if (bd == null) {
			bd = new BaseDeDatos();
		}
		return bd;
	}

	/** Cierra la conexi�n con la base de datos */
	public void cierraConexion() {
		try {
			conexion.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public ResultSet buscarEntidad(String consulta) {
		// TODO Auto-generated method stub
		ResultSet rs = null;
		try {
			// Se crea un Statement, para realizar la consulta
			Statement s = conexion.createStatement();

			// Se realiza la consulta. Los resultados se guardan en el
			// ResultSet rs
			rs = s.executeQuery(consulta);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return rs;
	}

	public ResultSet buscarParcial(String valor, String consulta) {
		// TODO Auto-generated method stub
		ResultSet rs = null;

		valor = valor.concat("%");

		try {
			// Se crea un Statement, para realizar la consulta
			// String consulta="select * from proveedores where cuit like ?";

			PreparedStatement s = conexion.prepareStatement(consulta);
			s.setString(1, valor);

			// Se realiza la consulta. Los resultados se guardan en el
			// ResultSet rs
			rs = s.executeQuery();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return rs;
	}

	public ResultSet consultar(String consulta) {

		ResultSet rs;
		try {
			rs = this.getConexion().prepareStatement(consulta).executeQuery();
			return rs;
		} catch (SQLException e) {

			e.printStackTrace();
			return null;
		}
	}

	public ResultSet consultarParametro(String consulta, Integer par) throws SQLException {
		ResultSet rs;
		PreparedStatement s = null;
		this.conexion.createStatement();
		s = conexion.prepareStatement(consulta);
		s.setInt(1, par);
		rs = s.executeQuery();
		return rs;
	}

	public boolean altaEntidad(String consulta, ArrayList<String> parametros) {
		// TODO Auto-generated method stub
		PreparedStatement s = null;
		boolean resultado = false;
		try {

			s = conexion.prepareStatement(consulta);
			int i = 0;
			while (i < parametros.size()) {
				s.setString(i + 1, parametros.get(i));
				i++;
			}

			s.execute();
			resultado = true;

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		;
		return resultado;
	}

	public Connection getConexion() {
		return conexion;
	}

	public void setConexion(Connection conexion) {
		this.conexion = conexion;
	}

	public static BaseDeDatos getBd() {
		return bd;
	}

	public static void setBd(BaseDeDatos bd) {
		BaseDeDatos.bd = bd;
	}

}
