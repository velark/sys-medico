package Modelo;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.swing.table.DefaultTableModel;

public class Usuario {

	private Integer dni;
	private String nombreUsuario;
	private String nombre;
	private String apellido;
	private String contrasenia;
	private String email;
	private String telefono;
	private String domicilio;
	private String localidad;
	private Integer tipoUsuario;
	
	private static Usuario bd = null;

	public Usuario(Integer dni, String nombreUsuario,  String nombre, String apellido,String contrasenia, String email,
			String telefono, String domicilio, String localidad, Integer tipoUsuario) {
		super();
		this.dni = dni;
		this.nombreUsuario = nombreUsuario;
		this.contrasenia = contrasenia;
		this.nombre = nombre;
		this.apellido = apellido;
		this.email = email;
		this.telefono = telefono;
		this.domicilio = domicilio;
		this.localidad = localidad;
		this.tipoUsuario = tipoUsuario;

	}

	public Usuario() {

	}
	
	public Usuario(String nombreUsuario, String Contrasenia) {
		
	}

	public static Usuario getInstance() {

		if (bd == null) {
			bd = new Usuario();
		}
		return bd;

	}

	// LOGIN
	public Boolean login(String nombreUsuario, String contrasenia) {
		ResultSet rs;
		try {

			rs = BaseDeDatos.getInstance().consultar("select * from Usuario where nombreUsuario =\'" + nombreUsuario
					+ "\' and contrasenia=\'" + contrasenia + "\'");
			if (rs.next()) {

				return true;
			}
		} catch (SQLException e) {
			// TODO Bloque catch generado autom�ticamente
			e.printStackTrace();
		}

		return false;

	}
	
	// OBTENER TIPO DE USUARIO

	public Integer obtenerTipoUsuario(String nombreUsuario) {
		ResultSet rs;
		try {
			rs = BaseDeDatos.getInstance().consultar(
					"select tipoUsuario_idtipoUsuario from Usuario where nombreUsuario=\'" + nombreUsuario + "\'");
			Integer tipo = null;
			while (rs.next()) {
				tipo = rs.getInt("tipoUsuario_idtipoUsuario");
			}
			return tipo;
		} catch (SQLException e) {
			// TODO Bloque catch generado autom�ticamente
			e.printStackTrace();
		}
		return null;
	}

	// ALTA USUARIOS

	public boolean altaUsuario(Usuario c) {

		PreparedStatement s = null;
		boolean resultado = false;
		try {
			// Se crea un Statement, para realizar la consulta

			// Se realiza la consulta parametrizada

			String nombreUsuario = c.getNombreUsuario();
			String contrasenia = c.getContrasenia();
			String nombre = c.getNombre();
			String apellido = c.getApellido();
			Integer dni = c.getDni();
			String email = c.getEmail();
			String domicilio = c.getDomicilio();
			String localidad = c.getLocalidad();
			String telefono = c.getTelefono();
			Integer tipoUsuario = c.getTipoUsuario();
			Integer baja = 0;

			String consulta = "insert into Usuario (idUsuario,nombreUsuario,nombre,apellido,contrasenia,dni,email,domicilio,localidad,telefono,tipoUsuario_idtipoUsuario,baja) values (null,?,?,?,?,?,?,?,?,?,?,?)";

			s = BaseDeDatos.getInstance().getConexion().prepareStatement(consulta);

			s.setString(1, nombreUsuario);
			s.setString(2, nombre);
			s.setString(3, apellido);
			s.setString(4, contrasenia);
			s.setInt(5, dni);
			s.setString(6, email);
			s.setString(7, domicilio);
			s.setString(8, localidad);
			s.setString(9, telefono);
			s.setInt(10, tipoUsuario);
			s.setInt(11, baja);

			s.execute();
			resultado = true;

		} catch (Exception e) {
			e.printStackTrace();
		}
		return resultado;
	}
	// ELIMINAR UN USUARIO
	
	public boolean eliminarUsuario(String nombre) throws SQLException {
		// TODO Auto-generated method stub
		PreparedStatement s = null;
		boolean resultado = false;
		String consulta = "update Usuario set baja = '1'  where nombre = ? "; 
		
		s = BaseDeDatos.getInstance().getConexion().prepareStatement(consulta);
		s.setString(1, nombre);
		s.execute();
		resultado = true;
		return resultado;
	}
	
	//ACTUALIZAR UN USUARIO
	
	public Boolean modificarUsuario(Usuario u, Integer id) {
		PreparedStatement s = null;
		Boolean resultado = false;

		String consulta = "update Usuario set nombreUsuario=?,nombre=?,apellido=?,contrasenia=?,dni=?,email=?,domicilio=?,localidad=?,telefono=?,tipoUsuario_idtipoUsuario=? where idmateria ="+ id;
		try{	
		s = BaseDeDatos.getInstance().getConexion().prepareStatement(consulta);
		s.setString(1, nombreUsuario);
		s.setString(2, nombre);
		s.setString(3, apellido);
		s.setString(4, contrasenia);
		s.setInt(5, dni);
		s.setString(6, email);
		s.setString(7, domicilio);
		s.setString(8, localidad);
		s.setString(9, telefono);
		s.setInt(10, tipoUsuario);
		
		s.executeUpdate();
		resultado = true;

		} catch(Exception e){e.printStackTrace();}
		return resultado;

	}
	// OBTENR ID USUARIO
	
	public Integer obteneridUsuario(String dni) {
		PreparedStatement s = null;
		Integer idUsuario = null;
		String consulta = "select idUsuario from Usuario  where dni = ?";
		try {
			s = BaseDeDatos.getInstance().getConexion().prepareStatement(consulta);
			s.setString(1, dni);
			ResultSet rs = s.executeQuery();
			while (rs.next()) {
				idUsuario = rs.getInt(1);
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return idUsuario;

	}
	// OBTENER DATOS USUARIO
	
	public Usuario datosUsuario(Integer id) {
		PreparedStatement s = null;
		Usuario resu = new Usuario();
		String consulta = "select dni,nombreUsuario,nombre,apellido,contrasenia,email,telefono,domicilio,localidad, tipoUsuario_idTipoUsuario from Usuario where idusuario = ? ";
		try {
			s = BaseDeDatos.getInstance().getConexion().prepareStatement(consulta);
			s.setInt(1, id);

			ResultSet rs = s.executeQuery();
			while (rs.next()) {
				Integer dni = rs.getInt(1);
				String nombreUsuario = rs.getString(2);
				String nombre = rs.getString(3);
				String apellido = rs.getString(4);
				String contrasenia = rs.getString(5);
				String email = rs.getString(6);
				String domicilio = rs.getString(7);
				String localidad = rs.getString(8);
				String telefono = rs.getString(9);
				Integer tipoUsuario = rs.getInt(10);
				
				
				
				
				
				resu = new Usuario( dni,contrasenia,nombre,apellido,nombreUsuario, email, domicilio, localidad,telefono, tipoUsuario);
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		 return resu;

	}





	// ACTUALIZAR TABLA USUARIOS

	public DefaultTableModel actualizarTablaUsuario(String busqueda) {
		
		String titulos[] = { "nombre", "apellido", "DNI", "Tipo de Usuario" };
		DefaultTableModel modelo = new DefaultTableModel(null, titulos);
		Object fila[] = new Object[4];
		if (busqueda.isEmpty()) {
			try {
				ResultSet consulta = BaseDeDatos.getInstance()
						.consultar("select nombre,apellido, dni, tipoUsuario_idtipoUsuario from Usuario where baja = '0'");
				while (consulta.next()) {
					fila[0] = consulta.getString("nombre");
					fila[1] = consulta.getString("apellido");
					fila[2] = consulta.getString("dni");
					fila[3] = consulta.getString("tipoUsuario_idtipoUsuario");
					if (fila[3].toString().equals("1"))
						fila[3] = "Administrador";
					else if (fila[3].toString().equals("2")) {
						fila[3] = "Medico";
					} else {
						fila[3] = "Recepcionista";
					}

					modelo.addRow(fila);

				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return modelo;
		} else {
			try {
				String conca;
				conca = ("%" + busqueda + "%");
				PreparedStatement stm = BaseDeDatos.getInstance().getConexion().prepareStatement(
						"select nombre,apellido, dni, tipoUsuario_idtipoUsuario from Usuario where (nombre like ?) or (apellido like ?) or (dni like ?) or (tipoUsuario_idtipoUsuario like ?)");
				stm.setString(1, conca);
				stm.setString(2, conca);
				stm.setString(3, busqueda);
				stm.setString(4, busqueda);

				System.out.println(stm.toString());
				ResultSet consulta = stm.executeQuery();
				while (consulta.next()) {
					fila[0] = consulta.getString("nombre");
					fila[1] = consulta.getString("apellido");
					fila[2] = consulta.getString("dni");
					fila[3] = consulta.getString("tipoUsuario");

					modelo.addRow(fila);

				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
		return modelo;
	}

	public Integer getDni() {
		return dni;
	}

	public void setDni(Integer dni) {
		this.dni = dni;
	}

	public String getNombreUsuario() {
		return nombreUsuario;
	}

	public void setNombreUsuario(String nombreUsuario) {
		this.nombreUsuario = nombreUsuario;
	}

	public String getContrasenia() {
		return contrasenia;
	}

	public void setContrasenia(String contrasenia) {
		this.contrasenia = contrasenia;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public String getDomicilio() {
		return domicilio;
	}

	public void setDomicilio(String domicilio) {
		this.domicilio = domicilio;
	}

	public String getLocalidad() {
		return localidad;
	}

	public void setLocalidad(String localidad) {
		this.localidad = localidad;
	}

	public Integer getTipoUsuario() {
		return tipoUsuario;
	}

	public void setTipoUsuario(Integer tipoUsuario) {
		this.tipoUsuario = tipoUsuario;
	}

	@Override
	public String toString() {
		return "Usuario [dni=" + dni + ", nombreUsuario=" + nombreUsuario + ", contrasenia=" + contrasenia + ", nombre="
				+ nombre + ", apellido=" + apellido + ", email=" + email + ", telefono=" + telefono + ", domicilio="
				+ domicilio + ", localidad=" + localidad + ", tipoUsuario=" + tipoUsuario + "]";
	}

}
