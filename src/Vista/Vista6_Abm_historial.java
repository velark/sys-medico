package Vista;

import javax.swing.JPanel;

import Controlador.Controlador6_Abm_historial;

import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.JButton;

public class Vista6_Abm_historial extends JPanel {
    
	private Controlador6_Abm_historial controlador_vista_6;
    private JTable table;
    private JButton button;
	
	
	public Vista6_Abm_historial(Controlador6_Abm_historial controlador) {
		this.setControlador_vista_6(controlador);
		setLayout(null);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(18, 64, 323, 225);
		add(scrollPane);
		
		table = new JTable();
		table.setModel(new DefaultTableModel(
			new Object[][] {
			},
			new String[] {
				"D.N.I", "Nombre", "Apellido", "Obra Social"
			}
		));
		scrollPane.setViewportView(table);
		
		JLabel lblPacientesDeDia = new JLabel("Pacientes de dia");
		lblPacientesDeDia.setBounds(167, 22, 91, 14);
		add(lblPacientesDeDia);
		
		button = new JButton("Historial");
		button.setBounds(351, 141, 89, 53);
		add(button);

	}


	public Controlador6_Abm_historial getControlador_vista_6() {
		return controlador_vista_6;
	}


	public void setControlador_vista_6(Controlador6_Abm_historial controlador_vista_6) {
		this.controlador_vista_6 = controlador_vista_6;
	}


	public JTable getTable() {
		return table;
	}


	public void setTable(JTable table) {
		this.table = table;
	}


	public JButton getButton() {
		return button;
	}


	public void setButton(JButton button) {
		this.button = button;
	}


}

