package Vista;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import Controlador.Controlador6_Abm_historial;
import Controlador.Controlador9_ABM_historial;

import java.awt.GridLayout;

public class Vista9_ABM_historial extends JPanel {
	
	private JTable table;
    private Controlador9_ABM_historial controlador;
	
	public Vista9_ABM_historial(Controlador9_ABM_historial controlador) {
		this.setControlador(controlador);
		
		setLayout(null);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 30, 477, 247);
		add(scrollPane);
		
		table = new JTable();
		table.setModel(new DefaultTableModel(
			new Object[][] {
			},
			new String[] {
				"Fecha de Visita", "Diagnostico", "Tratamiento", "Observaciones", "Historial Familiar"
			}
		));
		scrollPane.setViewportView(table);
		
		JPanel panel = new JPanel();
			panel.setBounds(497, 30, 88, 247);
		add(panel);
		panel.setLayout(new GridLayout(0, 1, 0, 0));
		
		JButton btnNewButton = new JButton("A\u00F1adir");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		panel.add(btnNewButton);
		
		JButton btnNewButton_1 = new JButton("Modificar");
		panel.add(btnNewButton_1);
		
		JButton btnNewButton_2 = new JButton("Eliminar");
		btnNewButton_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		panel.add(btnNewButton_2);

	}

	public JTable getTable() {
		return table;
	}

	public void setTable(JTable table) {
		this.table = table;
	}

	public Controlador9_ABM_historial getControlador() {
		return controlador;
	}

	public void setControlador(Controlador9_ABM_historial controlador) {
		this.controlador = controlador;
	}


}
