package Vista;

import java.awt.GridLayout;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import Controlador.Controlador10_Cargar_usuario;
import Controlador.Controlador13_Modificar_usuario;

public class Vista13_Modificar_usuario extends JPanel {

	private JComboBox comboTipoUsuario;
	private JTextField txtnombreUsuario;
	private JTextField txtApellido;
	private JTextField txtNombre;
	private JTextField txtDNI;
	private JTextField txtDomicilio;
	private JTextField txtLocalidad;
	private JTextField txtContrasenia;
	private JTextField txtTelefono;
	private JTextField txtEmail;
	private JButton btnAceptar;
	private Controlador13_Modificar_usuario controladorMU;

	public Vista13_Modificar_usuario(Controlador13_Modificar_usuario controladorMU) {

		this.setControladorMU(controladorMU);
		setLayout(new GridLayout(0, 4, 0, 0));
		this.setName("MODIFICAR_U");

		JLabel lblNuevoUsuario = new JLabel("      MODIFICAR USUARIO");
		add(lblNuevoUsuario);

		JLabel label = new JLabel("");
		add(label);

		JLabel label_1 = new JLabel("");
		add(label_1);

		JLabel label_2 = new JLabel("");
		add(label_2);

		JLabel label_3 = new JLabel("");
		add(label_3);

		JLabel label_4 = new JLabel("");
		add(label_4);

		JLabel label_5 = new JLabel("");
		add(label_5);

		JLabel label_6 = new JLabel("");
		add(label_6);

		JLabel lblNombreDeUsuario = new JLabel("      Usuario");
		add(lblNombreDeUsuario);

		txtnombreUsuario = new JTextField("");
		add(txtnombreUsuario);

		JLabel lblContrasea = new JLabel("      Contraseña");
		add(lblContrasea);

		txtContrasenia = new JTextField("");
		add(txtContrasenia);

		JLabel lblApellido = new JLabel("      Apellido");
		add(lblApellido);

		txtApellido = new JTextField("");
		add(txtApellido);

		JLabel lblTelefono = new JLabel("      Telefono");
		add(lblTelefono);

		txtTelefono = new JTextField("");
		add(txtTelefono);

		JLabel lblNombre = new JLabel("      Nombre");
		add(lblNombre);

		txtNombre = new JTextField("");
		add(txtNombre);

		JLabel lblTipoDeUsuario = new JLabel("      Tipo de usuario");
		add(lblTipoDeUsuario);

		comboTipoUsuario = new JComboBox();
		comboTipoUsuario
				.setModel(new DefaultComboBoxModel(new String[] { "Administrador", "Recepcionista", "Medico" }));
		add(comboTipoUsuario);

		JLabel lblDni = new JLabel("      DNI");
		add(lblDni);

		txtDNI = new JTextField("");
		add(txtDNI);

		JLabel lblEmail = new JLabel("      Email");
		add(lblEmail);

		txtEmail = new JTextField("");
		add(txtEmail);

		JLabel lblDomicilio = new JLabel("      Domicilio");
		add(lblDomicilio);

		txtDomicilio = new JTextField("");
		add(txtDomicilio);

		JLabel label_16 = new JLabel("");
		add(label_16);

		JLabel label_21 = new JLabel("");
		add(label_21);

		JLabel lblLocalidad = new JLabel("      Localidad");
		add(lblLocalidad);

		txtLocalidad = new JTextField("");
		add(txtLocalidad);

		JLabel label_23 = new JLabel("");
		add(label_23);

		btnAceptar = new JButton("Aceptar");
		btnAceptar.addActionListener(getControladorMU());
		add(btnAceptar);

		JLabel label_24 = new JLabel("");
		add(label_24);

		JLabel label_25 = new JLabel("");
		add(label_25);

		JLabel label_26 = new JLabel("");
		add(label_26);

	}

	public JComboBox getComboTipoUsuario() {
		return comboTipoUsuario;
	}

	public void setComboTipoUsuario(JComboBox comboTipoUsuario) {
		this.comboTipoUsuario = comboTipoUsuario;
	}

	public JTextField getTxtnombreUsuario() {
		return txtnombreUsuario;
	}

	public void setTxtnombreUsuario(JTextField txtnombreUsuario) {
		this.txtnombreUsuario = txtnombreUsuario;
	}

	public JTextField getTxtNombre() {
		return txtApellido;
	}

	public void setTxtNombre(JTextField txtNombre) {
		this.txtApellido = txtNombre;
	}

	public JTextField getTxtNombre1() {
		return txtNombre;
	}

	public void setTxtNombre1(JTextField txtNombre1) {
		this.txtNombre = txtNombre1;
	}

	public JTextField getTxtDNI() {
		return txtDNI;
	}

	public void setTxtDNI(JTextField txtDNI) {
		this.txtDNI = txtDNI;
	}

	public JTextField getTxtDomicilio() {
		return txtDomicilio;
	}

	public void setTxtDomicilio(JTextField txtDomicilio) {
		this.txtDomicilio = txtDomicilio;
	}

	public JTextField getTxtLocalidad() {
		return txtLocalidad;
	}

	public void setTxtLocalidad(JTextField txtLocalidad) {
		this.txtLocalidad = txtLocalidad;
	}

	public JTextField getTxtContrasenia() {
		return txtContrasenia;
	}

	public void setTxtContrasenia(JTextField txtContrasenia) {
		this.txtContrasenia = txtContrasenia;
	}

	public JTextField getTxtFechaNac() {
		return txtTelefono;
	}

	public void setTxtFechaNac(JTextField txtFechaNac) {
		this.txtTelefono = txtFechaNac;
	}

	public JTextField getTxtEmail() {
		return txtEmail;
	}

	public void setTxtEmail(JTextField txtEmail) {
		this.txtEmail = txtEmail;
	}

	public JButton getBtnAceptar() {
		return btnAceptar;
	}

	public void setBtnAceptar(JButton btnAceptar) {
		this.btnAceptar = btnAceptar;
	}

	public JTextField getTxtApellido() {
		return txtApellido;
	}

	public void setTxtApellido(JTextField txtApellido) {
		this.txtApellido = txtApellido;
	}

	public JTextField getTxtTelefono() {
		return txtTelefono;
	}

	public void setTxtTelefono(JTextField txtTelefono) {
		this.txtTelefono = txtTelefono;
	}

	public Controlador13_Modificar_usuario getControladorMU() {
		return controladorMU;
	}

	public void setControladorMU(Controlador13_Modificar_usuario controladorMU) {
		this.controladorMU = controladorMU;
	}

}
