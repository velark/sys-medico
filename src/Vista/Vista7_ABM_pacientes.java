package Vista;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

import Controlador.Controlador4_ABM_usuarios;
import Controlador.Controlador7_ABM_pacientes;
import java.awt.GridLayout;

public class Vista7_ABM_pacientes extends JPanel {

	private Controlador7_ABM_pacientes controladorPacientes;
	private JTextField txtBusqueda;
	private JButton btnCargar;
	private JButton btnModificar;
	private JButton btnEliminar;
	private JTable tablaTurnos;
	private JPanel panelABM;
	private JButton btnBuscar;

	public Vista7_ABM_pacientes(Controlador7_ABM_pacientes controladorPacientes) {
		this.setControladorPacientes(controladorPacientes);

		setLayout(null);

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(6, 60, 445, 380);
		add(scrollPane);

		tablaTurnos = new JTable();
		tablaTurnos.setModel(new DefaultTableModel(new Object[][] {},
				new String[] { "Dni", "nombre", "apellido", "Obra Social" }));
		tablaTurnos.setFont(new java.awt.Font("Tahoma", 2, 20));
		
		scrollPane.setViewportView(tablaTurnos);

		txtBusqueda = new JTextField();
		txtBusqueda.addActionListener(getControladorPacientes());
		txtBusqueda.setBounds(139, 4, 201, 42);
		add(txtBusqueda);
		txtBusqueda.setColumns(10);
		
		panelABM = new JPanel();
		panelABM.setBounds(455, 285, 103, 155);
		add(panelABM);
				panelABM.setLayout(new GridLayout(0, 1, 0, 0));
		
				btnCargar = new JButton("Cargar");
				panelABM.add(btnCargar);
				
						btnEliminar = new JButton("Eliminar");
						panelABM.add(btnEliminar);
						
								btnModificar = new JButton("Modificar");
								panelABM.add(btnModificar);
								
								btnBuscar = new JButton("Buscar");
								btnBuscar.setBounds(6, 6, 117, 42);
								add(btnBuscar);
								btnModificar.addActionListener(getControladorPacientes());
						btnEliminar.addActionListener(getControladorPacientes());

		btnCargar.addActionListener(getControladorPacientes());

	}

	public JTextField getTxtBusqueda() {
		return txtBusqueda;
	}

	public void setTxtBusqueda(JTextField txtBusqueda) {
		this.txtBusqueda = txtBusqueda;
	}

	public JButton getBtnCargar() {
		return btnCargar;
	}

	public void setBtnCargar(JButton btnCargar) {
		this.btnCargar = btnCargar;
	}

	public JButton getBtnModificar() {
		return btnModificar;
	}

	public void setBtnModificar(JButton btnModificar) {
		this.btnModificar = btnModificar;
	}

	public JButton getBtnEliminar() {
		return btnEliminar;
	}

	public void setBtnEliminar(JButton btnEliminar) {
		this.btnEliminar = btnEliminar;
	}

	public Controlador7_ABM_pacientes getControladorPacientes() {
		return controladorPacientes;
	}

	public void setControladorPacientes(Controlador7_ABM_pacientes controladorPacientes) {
		this.controladorPacientes = controladorPacientes;
	}
}
