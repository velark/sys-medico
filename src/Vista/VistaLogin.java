package Vista;

import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import Controlador.Controlador2_Login;
import java.awt.GridLayout;

public class VistaLogin extends JDialog {

	private final JPanel contentPanel = new JPanel();
	private Controlador2_Login controladorLogin;
	private JTextField txtNombreUsuario;
	private JTextField txtPassword;
	private JButton BtnAceptar;
	private JButton BtnCancelar;


	public VistaLogin(Controlador2_Login controladorLogin) {
		this.setControladorLogin(controladorLogin);
		setBounds(100, 100, 450, 300);
		getContentPane().setLayout(null);
		contentPanel.setBounds(0, 0, 0, 0);
		contentPanel.setLayout(new FlowLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel);
		{
			JLabel lblNombreDeUsuario = new JLabel("Nombre de Usuario");
			lblNombreDeUsuario.setBounds(65, 150, 121, 16);
			getContentPane().add(lblNombreDeUsuario);
			
			JLabel lblPassword = new JLabel("Password");
			lblPassword.setBounds(65, 196, 61, 16);
			getContentPane().add(lblPassword);
			
			txtNombreUsuario = new JTextField();
			txtNombreUsuario.setBounds(198, 145, 182, 26);
			getContentPane().add(txtNombreUsuario);
			txtNombreUsuario.setColumns(10);
			
			txtPassword = new JTextField();
			txtPassword.setBounds(198, 191, 182, 26);
			getContentPane().add(txtPassword);
			txtPassword.setColumns(10);
			
			JButton btnImagen = new JButton("Imagen");
			btnImagen.setBounds(132, 6, 182, 132);
			getContentPane().add(btnImagen);
			
			
			JPanel buttonPane = new JPanel();
			buttonPane.setBounds(0, 224, 450, 54);
			getContentPane().add(buttonPane);
			{
				BtnAceptar = new JButton("Aceptar");
				BtnAceptar.addActionListener(getControladorLogin());
				buttonPane.setLayout(new GridLayout(0, 2, 0, 0));
				BtnAceptar.setActionCommand("Aceptar");
				buttonPane.add(BtnAceptar);
				getRootPane().setDefaultButton(BtnAceptar);
			}
			{
				BtnCancelar = new JButton("Cancelar");
				BtnCancelar.addActionListener(getControladorLogin());
				BtnCancelar.setActionCommand("Cancelar");
				buttonPane.add(BtnCancelar);
			}
		}
	}


	public Controlador2_Login getControladorLogin() {
		return controladorLogin;
	}


	public void setControladorLogin(Controlador2_Login controladorLogin) {
		this.controladorLogin = controladorLogin;
	}


	public JTextField getTxtNombreUsuario() {
		return txtNombreUsuario;
	}


	public void setTxtNombreUsuario(JTextField txtNombreUsuario) {
		this.txtNombreUsuario = txtNombreUsuario;
	}


	public JTextField getTxtPassword() {
		return txtPassword;
	}


	public void setTxtPassword(JTextField txtPassword) {
		this.txtPassword = txtPassword;
	}


	public JButton getBtnAceptar() {
		return BtnAceptar;
	}


	public void setBtnAceptar(JButton btnAceptar) {
		BtnAceptar = btnAceptar;
	}


	public JButton getBtnCancelar() {
		return BtnCancelar;
	}


	public void setBtnCancelar(JButton btnCancelar) {
		BtnCancelar = btnCancelar;
	}

}
