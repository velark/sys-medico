package Controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

import javax.swing.JOptionPane;

import Modelo.Usuario;
import Vista.Vista10_Cargar_usuario;

public class Controlador10_Cargar_usuario implements ActionListener {

	private Vista10_Cargar_usuario vistaCargarUsuario;
	private Usuario usuario;

	public Controlador10_Cargar_usuario() {

		this.setVistaCargarUsuario(new Vista10_Cargar_usuario(this));
		this.setUsuario(usuario);
		
		
	}

	@Override
	public void actionPerformed(ActionEvent e) {

		if (e.getSource() == this.getVistaCargarUsuario().getBtnAceptar()) {

			System.out.println("Verifica campos vacios");

			if (this.getVistaCargarUsuario().getTxtDNI().getText().isEmpty()
					|| this.getVistaCargarUsuario().getTxtContrasenia().getText().isEmpty()
					|| this.getVistaCargarUsuario().getTxtnombreUsuario().getText().isEmpty()
					|| this.getVistaCargarUsuario().getTxtNombre().getText().isEmpty()
					|| this.getVistaCargarUsuario().getTxtApellido().getText().isEmpty()
					|| this.getVistaCargarUsuario().getTxtEmail().getText().isEmpty()
					|| this.getVistaCargarUsuario().getTxtDomicilio().getText().isEmpty()
					|| this.getVistaCargarUsuario().getTxtLocalidad().getText().isEmpty()
					|| this.getVistaCargarUsuario().getTxtTelefono().getText().isEmpty()
					|| this.getVistaCargarUsuario().getComboTipoUsuario().getActionCommand().isEmpty()) {

				JOptionPane.showMessageDialog(this.getVistaCargarUsuario(), "Todos los campos son obligatorios");

			} else {

				// -------------------------------CREA EL
				// USUARIO----------------------------------

				Integer tipo;
				if (this.getVistaCargarUsuario().getComboTipoUsuario().getSelectedItem().toString().equals("Administrador")){

					tipo = 1;
				}
				else if (this.getVistaCargarUsuario().getComboTipoUsuario().getSelectedItem().toString().equals("Medico")){

					tipo = 2;
				} else {
					tipo = 3;
				}
				
				Usuario usuario = new Usuario(Integer.valueOf(this.getVistaCargarUsuario().getTxtDNI().getText()),
						this.getVistaCargarUsuario().getTxtContrasenia().getText(),
						this.getVistaCargarUsuario().getTxtnombreUsuario().getText(),
						this.getVistaCargarUsuario().getTxtNombre().getText(),
						this.getVistaCargarUsuario().getTxtApellido().getText(),
						this.getVistaCargarUsuario().getTxtEmail().getText(),
						this.getVistaCargarUsuario().getTxtTelefono().getText(),
						this.getVistaCargarUsuario().getTxtDomicilio().getText(),
						this.getVistaCargarUsuario().getTxtLocalidad().getText(), tipo);

				try {
					System.out.println("carga nuevo usuario");
					Boolean creado = Usuario.getInstance().altaUsuario(usuario);

					if (creado) {
						JOptionPane.showMessageDialog(getVistaCargarUsuario(), "Usuario creado con exito");
					} else {
						JOptionPane.showMessageDialog(getVistaCargarUsuario(), "El usuario no ha sido creado");
					}
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					JOptionPane.showMessageDialog(getVistaCargarUsuario(), "ERROR: El DNI ya existe");
				}

			}
			
			

		}

	}
	// TODO Auto-generated method stub

	public Vista10_Cargar_usuario getVistaCargarUsuario() {
		return vistaCargarUsuario;
	}

	public void setVistaCargarUsuario(Vista10_Cargar_usuario vistaCargarUsuario) {
		this.vistaCargarUsuario = vistaCargarUsuario;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	

}
