package Controlador;

import java.awt.CardLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

import javax.swing.JPanel;

import Controlador.Controlador.CargarPanel;
import Modelo.Usuario;
import Vista.Vista1_PP;
import Vista.Vista3_ABM_turnos;

public class Controlador1_PP implements ActionListener, CargarPanel {

	private Vista1_PP vistaPrincipal;
	private Usuario usuario;

	public Controlador1_PP() {
		super();

		this.setVistaPrincipal(new Vista1_PP(this));

	}

	public Controlador1_PP(Usuario usuario, Integer tipo) {

		this.setVistaPrincipal(new Vista1_PP(this));
		this.setUsuario(usuario);
	}

	@Override
	public void actionPerformed(ActionEvent e) {

		if (e.getSource() == this.getVistaPrincipal().getBtnTurnos_1()) {
			System.out.println("pasa por aca... panel turnos");

			CardLayout cl = (CardLayout) this.getVistaPrincipal().getPanel_Card().getLayout();
			cl.show(this.getVistaPrincipal().getPanel_Card(), "TURNOS");

		}

		if (e.getSource() == this.getVistaPrincipal().getBtnUsuarios_1()) {
			System.out.println("pasa por aca... panel usuarios");

			CardLayout cl = (CardLayout) this.getVistaPrincipal().getPanel_Card().getLayout();
			cl.show(this.getVistaPrincipal().getPanel_Card(), "USUARIOS");

		}

		if (e.getSource() == this.getVistaPrincipal().getBtnMedicos()) {
			System.out.println("pasa por aca... panel medicos");

			CardLayout cl = (CardLayout) this.getVistaPrincipal().getPanel_Card().getLayout();
			cl.show(this.getVistaPrincipal().getPanel_Card(), "MEDICOS");

		}
		if (e.getSource() == this.getVistaPrincipal().getBtnPacientes()) {
			System.out.println("pasa por aca... panel pacientes");

			CardLayout cl = (CardLayout) this.getVistaPrincipal().getPanel_Card().getLayout();
			cl.show(this.getVistaPrincipal().getPanel_Card(), "PACIENTES");
		}
		if (e.getSource() == this.getVistaPrincipal().getBtnReportes()) {
			System.out.println("pasa por aca... panel reportes");

			CardLayout cl = (CardLayout) this.getVistaPrincipal().getPanel_Card().getLayout();
			cl.show(this.getVistaPrincipal().getPanel_Card(), "REPORTES");
		}

	}

	public Vista1_PP getVistaPrincipal() {
		return vistaPrincipal;
	}

	public void setVistaPrincipal(Vista1_PP vistaPrincipal) {
		this.vistaPrincipal = vistaPrincipal;
	}

	@Override
	public JPanel crearPanel(String nombre) {

		if (nombre.equals("TURNOS")) {
			Controlador3_ABM_turnos controladorTurnos = new Controlador3_ABM_turnos(this);
			JPanel panel = controladorTurnos.getVistaTurnos();

			return panel;

		}
		if (nombre.equals("USUARIOS")) {
			Controlador4_ABM_usuarios controladorU = new Controlador4_ABM_usuarios(this);
			JPanel panel = controladorU.getVistaUsuarios();

			return panel;
		}

		if (nombre.equals("REPORTES")) {
			Controlador8_reportes controladorR = new Controlador8_reportes();
			JPanel panel = controladorR.getVistaReportes();

			return panel;
		}
		if (nombre.equals("MEDICOS")) {
			Controlador5_ABM_medicos controladorM = new Controlador5_ABM_medicos();
			JPanel panel = controladorM.getVistaMedicos();

			return panel;
		}
		if (nombre.equals("PACIENTES")) {
			Controlador7_ABM_pacientes controladorP = new Controlador7_ABM_pacientes(this);
			JPanel panel = controladorP.getVistaPacientes();

			return panel;
		}
		if (nombre.equals("CARGAR_US")) {
			Controlador10_Cargar_usuario controladorCU = new Controlador10_Cargar_usuario();
			JPanel panel = controladorCU.getVistaCargarUsuario();

			return panel;
		}

		/// Asociar controlador turno con el controlador principal crea en panel
		if (nombre.equals("CARGAR_TU")) {
			Controlador11_cargar_turnos controladorTU = new Controlador11_cargar_turnos();
			JPanel panel = controladorTU.getVista();

			return panel;
		}
		if (nombre.equals("CARGAR_PA")) {
			Controlador12_Cargar_Paciente controladorPA = new Controlador12_Cargar_Paciente();
			JPanel panel = controladorPA.getVistaCP();

			return panel;
		}
		if (nombre.equals("MODIFICAR_U")) {
			Controlador13_Modificar_usuario controladorMU = new Controlador13_Modificar_usuario();
			JPanel panel = controladorMU.getVistaMU();

			return panel;
		}

		return null;
	}

	public Vista1_PP vistaTipoUsuario(Integer tipoUsuario) {

		System.out.println(" EL tipo de Usuario Logueado es: " + tipoUsuario.toString());
		
		switch (tipoUsuario) {
		case 1:
			Vista1_PP vistaUsuario = new Vista1_PP(this);

			this.getVistaPrincipal().getBtnTurnos_1().setEnabled(false);
			this.getVistaPrincipal().getBtnPacientes().setEnabled(false);
			this.getVistaPrincipal().setVisible(true);

			return vistaUsuario;
			
		case 2:
			Vista1_PP vistaMedico = new Vista1_PP(this);

			this.getVistaPrincipal().getBtnTurnos_1().setEnabled(false);
			//this.getVistaPrincipal().getBtnPacientes().setEnabled(false);
			this.getVistaPrincipal().getBtnMedicos().setEnabled(false); 
			this.getVistaPrincipal().getBtnUsuarios_1().setEnabled(false);
			this.getVistaPrincipal().setVisible(true);

			return vistaMedico;
			
		case 3:
			
			Vista1_PP vistaRecepcionista = new Vista1_PP(this);

			//this.getVistaPrincipal().getBtnTurnos_1().setEnabled(false);
			//this.getVistaPrincipal().getBtnPacientes().setEnabled(false);
			this.getVistaPrincipal().getBtnUsuarios_1().setEnabled(false);
			this.getVistaPrincipal().getBtnMedicos().setEnabled(false); 			
			this.getVistaPrincipal().setVisible(true);
			
			return vistaRecepcionista;
			
		default:
			break;
		}
		return vistaPrincipal;

	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
}
