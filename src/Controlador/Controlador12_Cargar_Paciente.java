package Controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import javax.swing.JOptionPane;

import Modelo.Paciente;
import Modelo.Usuario;
import Vista.Vista12_Cargar_paciente;

public class Controlador12_Cargar_Paciente implements ActionListener {
	
	private Vista12_Cargar_paciente vistaCP;
	
	public Controlador12_Cargar_Paciente() {
		
		this.setVistaCP(new Vista12_Cargar_paciente(this));
	}
	
	

	@Override
	public void actionPerformed(ActionEvent e) {
		
		if (e.getSource() == this.getVistaCP().getBtnAceptar()) {

			System.out.println("Verifica campos vacios");

			if (
					 this.getVistaCP().getTxtNombre().getText().isEmpty()
					|| this.getVistaCP().getTxtApellido().getText().isEmpty()
					|| this.getVistaCP().getTxtDNI().getText().isEmpty()
					|| this.getVistaCP().getTxtFechaNac().getText().isEmpty()
					|| this.getVistaCP().getTxtEmail().getText().isEmpty()
					|| this.getVistaCP().getTxtDomicilio().getText().isEmpty()
					|| this.getVistaCP().getTxtLocalidad().getText().isEmpty()
					|| this.getVistaCP().getTxtTelefono().getText().isEmpty()
					|| this.getVistaCP().getComboSexo().getActionCommand().isEmpty()
					|| this.getVistaCP().getComboObraSocial().getActionCommand().isEmpty()) {

				JOptionPane.showMessageDialog(this.getVistaCP(), "Todos los campos son obligatorios");

			} else {

				// -------------------------------CREA EL
				// USUARIO----------------------------------

				
				DateTimeFormatter formatter = DateTimeFormatter.ofPattern("d/MM/yyyy");
				
				Paciente paciente = new Paciente(this.getVistaCP().getTxtNombre().getText(),
						this.getVistaCP().getTxtApellido().getText(),
						Integer.valueOf(this.getVistaCP().getTxtDNI().getText()),
						this.getVistaCP().getTxtDomicilio().getText(),
						this.getVistaCP().getTxtLocalidad().getText(),
						LocalDate.parse(this.getVistaCP().getTxtFechaNac().getText(),formatter),
						this.getVistaCP().getComboObraSocial().getSelectedItem().toString(),
						this.getVistaCP().getComboSexo().getSelectedItem().toString(),
						this.getVistaCP().getTxtEmail().getText(),
						this.getVistaCP().getTxtTelefono().getText());

				try {
					System.out.println("carga nuevo paciente");
					Boolean creado = Paciente.getInstance().altaPaciente(paciente);

					if (creado) {
						JOptionPane.showMessageDialog(getVistaCP(), "Se ha cargado un nuevo Paciente con exito");
					} else {
						JOptionPane.showMessageDialog(getVistaCP(), "El Paciente no ha sido creado");
					}
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					JOptionPane.showMessageDialog(getVistaCP(), "ERROR: El DNI ya existe");
				}

			}
			
			

		}

	}
		// TODO Auto-generated method stub
		
	





	public Vista12_Cargar_paciente getVistaCP() {
		return vistaCP;
	}





	public void setVistaCP(Vista12_Cargar_paciente vistaCP) {
		this.vistaCP = vistaCP;
	}
	
	

}
